using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ScreenStruct : MonoBehaviour
{
    [SerializeField]
    public List<Animator> anims;

    public GameObject belongsToScreen;


    public void StartAnims(string trigger)
    {
        belongsToScreen.SetActive(true);
        foreach (var item in anims)
        {
            item.gameObject.SetActive(true);
            item.SetTrigger(trigger);
        }
    }
    public void UnStartAnims(string trigger)
    {
        foreach (var item in anims)
        {
            item.gameObject.SetActive(false);
            item.SetTrigger(trigger);
        }
    }
}
