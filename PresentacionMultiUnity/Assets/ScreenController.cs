using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ScreenController : MonoBehaviour
{
    [SerializeField]
    public List<ScreenStruct> screens;
    public int currScreen = 0;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            ShowNextScreen();
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            ShowBackScreen();
        }
    }
    public void ShowNextScreen()
    {
        if (currScreen < screens.Count)
        {
            screens[currScreen].StartAnims("Start");
            currScreen++;
        }
    }
    public void ShowBackScreen()
    {
        if (currScreen < screens.Count)
        {
            screens[currScreen].UnStartAnims("Back");
            currScreen--;
        }
    }
}
